from django.shortcuts import render,redirect
from django.http import HttpResponse
from myapp import categoryscraper,mendeleyScores
from mendeleyScores import *
#from django.shortcuts import render,redirect
#from django.core.files.storage import FileSystemStorage

# Create your views here.

def home(request):
    return render(request, 'WebsiteSEGP.html')
    
def about(request):
    return render(request, 'About.html')
    
def topic(request):
    return render(request, 'Topics.html')
    
def case1(request):
    return render(request, 'Case1.html')

def case2(request):
    return render(request, 'Case2.html')
    
def testing(request):
    submitted_category = []
    categories =[]
    if request.method == 'GET':
        query = str(request.GET['topics'])
        pub_query = 'publication' in request.GET
        acite_query = 'acite' in request.GET
        cite_query = 'cite' in request.GET
        categories = categoryscraper.categoryscraper(query)
        if (pub_query and cite_query):
            print("Publication and citation:" + str(pub_query)+", "+str(cite_query))
            """ perform calcluation in calculator script fucntion s1 """
            s1=s1(categories)
        else if (pub_query):
            print("Publication:" + str(pub_query))
            """ perform calcluation in calculator script fucntion s3 """
            s3=s3(categories)
        else if (acite_query):
            print("Author citation:" + str(acite_query))
            """  perform calcluation in calculator script, not yet implemented"""
        else if (cite_query):
            print("Ciatation:" + str(cite_query))
            """  perform calcluation in calculator script function s2"""
            s2=s2(categories)
    context = {
    
        'subcategories' :avgReaderCountScript.avgRCSList(categories)
        
    }
    return render(request, 'Testing.html',context)

def results1(request):
    submitted_query = []
    if request.method == 'POST':
        query = str(request.POST['input_submitted'])
        print(query)
        a = query.splitlines()
        for i in a:
             i=i.replace('× ', '')
             i=i.replace('×', '')
             if i !="":
                submitted_query.append((str(i)))
    context = avgReaderCountScript.avgRCSList(submitted_query)    
    return render(request, 'Results1.html',context)

def results2(request):
    
    return render(request, 'Results2.html')
    
    
    
    
